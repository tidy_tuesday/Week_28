
# Tidy Tuesday - Week 28
### US Voter Turnout

I was curious about low turnout this week so I focused on identifying states with historically turnout values. Much more can be done here though.

See https://github.com/rfordatascience/tidytuesday for more information.

#### US Voter Turnout
![](voter_turnout.png)

#### States with lowest turnout over last 9 presidential elections
![](pres_turnout.png)

#### States with lowest turnout over last 9 midterm elections
![](mid_turnout.png)
